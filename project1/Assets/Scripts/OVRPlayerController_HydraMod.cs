﻿using UnityEngine;
using System.Collections;

public class OVRPlayerController_HydraMod {


	#region ELLIOTT0
	//==========================================================================//
	
	//TODO: ELLIOTT ADDED
	private float leftHydraJoystickX;
	private float leftHydraJoystickY;
	private float rotationSpeed = 1f;
	float cutoff = 0.05f;
	//==========================================================================//
	#endregion

	public void SetMovement(out bool  moveForward, out bool moveBack, out bool moveLeft, out bool moveRight){

		#region ELLIOTT1
		//==========================================================================//
		//TODO: ELLIOTT REMOVED THIS
		/*bool moveForward = Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow);
		bool moveLeft = Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow);
		bool moveRight = Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow);
		bool moveBack = Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow);
*/
	
		//TODO: ELLIOTT ADDED
		leftHydraJoystickX = SixenseInput.Controllers [1].JoystickX;
		leftHydraJoystickY = SixenseInput.Controllers [1].JoystickY;
	
	
	
		moveForward = leftHydraJoystickY > cutoff;
		moveBack = leftHydraJoystickY < -cutoff;
	
		moveLeft = false;
		moveRight = false;
		//==========================================================================//
		#endregion




	}

	public void SetRotation(ref Vector3 euler,ref float rotateInfluence){

		euler.y+= leftHydraJoystickX * rotateInfluence *rotationSpeed;//use Hydra axis instead of mouse
	}






}
