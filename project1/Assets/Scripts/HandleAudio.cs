﻿using UnityEngine;
using System.Collections;

public class HandleAudio : MonoBehaviour
{
	
	public bool isInside = false;

	public bool wasInside = false;
	
//	public bool inCourtyard = false;
	public GameObject[] DoorAudioSources;
	public GameObject[] OutsideAudioSources;


	private float lpfMin = 3000f;
	//private float lpfStepSize = 10f;
	private float lpfMax = 20000f;

	private float volumeMin = 0.3f;
	private float volumeMax = 0.9f;
	//private float volumeStepSize = 0.1f;



	
	void LateUpdate ()
	{
		if (wasInside != isInside) {
			foreach (GameObject doorSource in DoorAudioSources) {
				///AudioLowPassFilter lpf = doorSource.GetComponent<AudioLowPassFilter>();
				AudioSource sound = doorSource.GetComponent<AudioSource> ();
				if (isInside) {
					sound.Play ();
				} else {
					sound.Stop ();
				}
				
			}

			foreach (GameObject outsideRain in OutsideAudioSources) {
				AudioSource sound = outsideRain.GetComponent<AudioSource> ();
				AudioLowPassFilter lpf = outsideRain.GetComponent<AudioLowPassFilter> ();

				if (isInside) {
					lpf.cutoffFrequency = lpfMin;
					sound.volume = volumeMin;
				} else {
					lpf.cutoffFrequency = lpfMax;
					sound.volume = volumeMax;
				}

			

			}
			wasInside = isInside;

		}
	}



	void OnTriggerEnter (Collider other)
	{
		
		if (other.name.Equals ("InsideRegion")) {
			wasInside = false;
			isInside = true;
		}
		if (other.name.Equals ("CourtyardRegion")) {
			wasInside = true;
			isInside = false;
			//inCourtyard = true;
		}
		
	}
	
	
	void OnTriggerExit (Collider other)
	{
	
		if (other.name.Equals ("InsideRegion")) {
			wasInside = true;
			isInside = false;
		}
		
		if (other.name.Equals ("CourtyardRegion")) {
			wasInside = false;
			isInside = true;
			//inCourtyard = false;
		}
		
	}
}
